// Code by M. Ozair Khatri <-- 20-03-2017 -->
// Connecting two HC-05's to Android and simultaneously receiving data from them.
package com.momsk.multibluetooth;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;
public class MainActivity extends Activity {
    BluetoothAdapter mbad;
    BluetoothDevice bd1,bd2;
    BluetoothSocket bs1,bs2;
    InputStream is1,is2;
    OutputStream os1,os2;
    TextView rcv_1,rcv_2;
    Boolean rcvfrm1 = true, rcvfrm2 = true;
    Handler h1 = new Handler();
    Handler h2 = new Handler();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mbad = BluetoothAdapter.getDefaultAdapter();
        bd1 = mbad.getRemoteDevice("98:D3:37:00:9D:68");
        bd2 = mbad.getRemoteDevice("98:D3:37:00:9D:2E");
        rcv_1 =(TextView) findViewById(R.id.hc1_rcv);
        rcv_2 =(TextView) findViewById(R.id.hc2_rcv);
        if(!mbad.isEnabled())
        {
            mbad.enable();
            while(!mbad.isEnabled());
        }
    }
    protected void con_1(View view)
    {
        try{
            bs1 = bd1.createInsecureRfcommSocketToServiceRecord(UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"));
            bs1.connect();
            is1 = bs1.getInputStream();
            os1 = bs1.getOutputStream();
        }catch (Exception e){
            Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
        }
    }
    protected void con_2(View view)
    {
        try{
            bs2 = bd2.createInsecureRfcommSocketToServiceRecord(UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"));
            bs2.connect();
            is2 = bs2.getInputStream();
            os2 = bs2.getOutputStream();
        }catch (Exception e){
            Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
        }
    }
    protected void rcv_1(View view)
    {
        Update1 u1 = new Update1();
        u1.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR," ");
    }
    protected void rcv_2(View view)
    {
        Update2 u2 = new Update2();
        u2.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR," ");
    }
    protected void appto1(char c) {
        rcv_1.setText(rcv_1.getText().toString() + c);
    }
    protected void appto2(char c) {
        rcv_2.setText(rcv_2.getText().toString() + c);
    }
    protected void clr_1(View view) {
        rcv_1.setText(" ");
        rcvfrm1 = false;
    }
    protected void clr_2(View view) {
        rcv_2.setText(" ");
        rcvfrm2 = false;
    }
    protected class Update1 extends AsyncTask<String, Void, String>
    {
        @Override
        protected String doInBackground(String... strings) {
            rcvfrm1 = true;
            while(rcvfrm1)
            {
                //
                try {
                    //Looper.prepare();
                    if (is1.available() > 0) {
                        h1.post(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    char c = (char) is1.read();
                                    appto1(c);
                                }catch (Exception e){}
                            }
                        });
                    }
                    //Log.d("inl","in loop");
                    Thread.sleep(20);
                } catch (Exception e) {
                    Toast.makeText(MainActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
                }
            }
            return null;
        }
    }
    protected class Update2 extends AsyncTask<String, Void, String>
    {
        @Override
        protected String doInBackground(String... strings) {
            rcvfrm2 = true;
            while(rcvfrm2)
            {
                //
                try {
                    //Looper.prepare();
                    if (is2.available() > 0) {
                        h2.post(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    char c = (char) is2.read();
                                    appto2(c);
                                }catch (Exception e){}
                            }
                        });
                    }
                    //Log.d("inl","in loop");
                    Thread.sleep(20);
                } catch (Exception e) {
                    Toast.makeText(MainActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
                }
            }
            return null;
        }
    }
}